package ru.kpfu.smarteducation.timetable.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {


    @RequestMapping("/timetable")
    public String getTimetable() {
        return "timetable";
    }

}