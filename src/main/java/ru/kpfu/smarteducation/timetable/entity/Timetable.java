package ru.kpfu.smarteducation.timetable.entity;

import java.util.HashMap;

public class Timetable {

    private int schoolDayNum;
    private int pairsPerDay;
    private int semester;

    private Timetable(final int schoolDayNum, final int pairsPerDay, final int semester,
                      final HashMap<Teacher, Integer> hoursForTeachers) {
        this.schoolDayNum = schoolDayNum;
        this.pairsPerDay = pairsPerDay;
        this.semester = semester;
        
    }

}
