package ru.kpfu.smarteducation.timetable.entity;

public class IUP {
    private Subject subject;
    private int semester;
    private int hours;

    public IUP(Subject subject, int semester, int hours) {
        this.subject = subject;
        this.semester = semester;
        this.hours = hours;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
}
