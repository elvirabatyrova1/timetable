package ru.kpfu.smarteducation.timetable.entity;

import java.util.List;

public class Teacher {
    private String fio;
    /**
     * Предметы, которые преподает
     */
    private List<Subject> subjects;
    /**
     * Сколько часов может преподавать в этом семестре
     */
    private int hours;
    /**
     * Какие компетенции развивает
     */
    private List<String> competences;
    private List<Wish> wishes;
}
