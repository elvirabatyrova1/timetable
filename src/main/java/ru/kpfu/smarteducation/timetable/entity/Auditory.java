package ru.kpfu.smarteducation.timetable.entity;

import java.util.List;

public class Auditory extends WishElement {
    private String name;
    private List<String> properties;
    /**
     * Какие компетенции развивает
     */
    private List<String> competencies;
}
