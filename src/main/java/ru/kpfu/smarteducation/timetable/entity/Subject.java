package ru.kpfu.smarteducation.timetable.entity;

import java.util.List;

public class Subject {
    private long subjectId;
    private String name;
    private List<String> competences;
    private List<Wish> wishes;
}
